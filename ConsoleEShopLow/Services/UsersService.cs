﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopLow.Interfaces;
using ConsoleEShopLow.Models;
using ConsoleEShopLow.Repository;

namespace ConsoleEShopLow.Services
{
   public class UsersService : IUserService
    {

        readonly IUserRepository _usersRepository;
        readonly ICurrentUser _currentUser;


        public UsersService(IUserRepository usersRepository, ICurrentUser currentUser)
        {
            _usersRepository = usersRepository;
            _currentUser = currentUser;
        }

        public bool Registration(string login, string password)
        {
            if (login == null || password == null)
            {
                
                return false;
            }        

            User user = new User { Login = login, Password = password, CurentRole = Role.User };

            _usersRepository.AddUser(user);

            return true;
        }
        public void ChandeUserInfo(string newLogin, string newPassword)
        {
            _currentUser.GetCurrentUser().Login = newLogin;
            _currentUser.GetCurrentUser().Password = newPassword;

        }
        public void AdminChandeUserInfo(int iD,string newLogin, string newPassword)
        {
            _usersRepository.GetUserById(iD).Login = newLogin;
            _usersRepository.GetUserById(iD).Password = newPassword;

        }
    }
}
