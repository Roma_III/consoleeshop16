﻿using ConsoleEShopLow.Interfaces;
using ConsoleEShopLow.Models;
using System.Collections.Generic;

namespace ConsoleEShopLow.Services
{
    public class ProductService : IProductService
    {
      
        readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public Product CreateProduct(string productName, Category productCategory, string productDescription, decimal price)
        {
            Product product = new Product
            {
                CurrentCategory = productCategory,
                Name = productName,
                Price = price,
                Description = productDescription
                
            };
            _productRepository.AddProduct(product);

            return product;

        }

        public Product EditProduct(int productId, string productName, Category productCategory, string productDescription, decimal price)
        {
            var currentProduct = _productRepository.GetProductById(productId);
            currentProduct.Name = productName;
            currentProduct.CurrentCategory = productCategory;
            currentProduct.Description = productDescription;
            currentProduct.Price = price;

            return currentProduct;
        }
        public IEnumerable<Product> GetProductsByName(string str)
        {
            return _productRepository.GetProduct(str);
        }
       
        public IEnumerable<Product> GetProducts()
        {
            return _productRepository.GetAllProducts();
        }

    }
}
