﻿using ConsoleEShopLow.Models;
using System.Collections.Generic;

namespace ConsoleEShopLow.Interfaces
{
    public interface IUserRepository
    {
        void AddUser(User user);
        User GetUser(string login);
        IEnumerable<User> GetUsers();
        User GetUserById(int userId);
    }
}
