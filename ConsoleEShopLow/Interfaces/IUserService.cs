﻿namespace ConsoleEShopLow.Interfaces
{
    public  interface IUserService
    {
        bool Registration(string login, string password);
        void ChandeUserInfo(string newLogin, string newPassword);
        void AdminChandeUserInfo(int iD, string newLogin, string newPassword);
    }
}
