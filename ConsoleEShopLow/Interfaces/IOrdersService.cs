﻿using ConsoleEShopLow.Models;
using System.Collections.Generic;

namespace ConsoleEShopLow.Interfaces
{
    public interface IOrdersService
    {
        IEnumerable<Order> GetMyOrders();
        IEnumerable<Order> GetOrdersForAdmin();
        void CreateOrder(Product product);
        Order GetOrderById(int orderId);
        void EditOrderStatus(int orderId, OrderStatus orderStatus);
    }
}
