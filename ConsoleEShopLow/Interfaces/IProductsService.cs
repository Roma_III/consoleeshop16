﻿using ConsoleEShopLow.Models;
using System.Collections.Generic;

namespace ConsoleEShopLow.Interfaces
{
    public interface IProductService

    {
        IEnumerable<Product> GetProducts();
        IEnumerable<Product> GetProductsByName(string str);
        Product CreateProduct(string productName, Category productCategory, string productDescription, decimal price);
        Product EditProduct(int productId, string productName, Category productCategory, string productDescription, decimal price);
    }
}
