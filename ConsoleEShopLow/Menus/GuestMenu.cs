﻿using ConsoleEShopLow.Interfaces;
using System;
using System.Linq;

namespace ConsoleEShopLow.Menus
{
    public class GuestMenu : IGuestMenu
    {
        readonly IloginMenu _loginMenu;
        readonly IRegistrationMenu _registrationMenu;
        readonly IProductService _productService;
        public GuestMenu(IloginMenu loginMenu, IRegistrationMenu registrationMenu, IProductService productService)
        {
            _loginMenu = loginMenu;
            _registrationMenu = registrationMenu;
            _productService = productService;
        }
        public void GuestMenuPrint()
        {
            Console.Clear();
            Console.WriteLine("----------------- Welcome  Guest ------------------");
            Console.WriteLine();
            Console.WriteLine("       If you want to register, press - 1");
            Console.WriteLine();
            Console.WriteLine("       If you want to log in, press - 2");
            Console.WriteLine();
            Console.WriteLine("       If you want to view products, press - 3");
            Console.WriteLine();
            Console.WriteLine("       If you want to search, press - 4");
            Console.WriteLine();
            Console.Write("Chose: ");

            string result;
            string[] possibleValues = new string[4] { "1", "2", "3", "4" };
            do
            {
                result = Console.ReadLine();
                Console.WriteLine();
                if (!possibleValues.Contains(result))
                {
                    Console.WriteLine(" Enter 1 or 2, or 3, or 4 !!!!");
                }
            } while (!possibleValues.Contains(result));

            if (result == "1")
            {
                _registrationMenu.Registration();
            }
            if (result == "2")
            {
                _loginMenu.Login();
            }
            if (result == "3")
            {
                _productService.GetProducts();
                
                foreach (var item in _productService.GetProducts())
                {
                    string product = (" | Name : " + item.Name  +" | Price : " + item.Price.ToString()+"$" + " | Category : " + item.CurrentCategory + " | Description : " + item.Description);
                    Console.WriteLine(product);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                     cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    GuestMenuPrint();
                }
                
            }
            if (result == "4")
            {
                Console.WriteLine();
                Console.Write("Search : ");
                string str = Console.ReadLine();
                Console.WriteLine();
                _productService.GetProductsByName(str);
                foreach (var item in _productService.GetProductsByName(str))
                {
                    string product = (" | Name : " + item.Name + " | Price : " + item.Price.ToString() + "$" + " | Category : " + item.CurrentCategory + " | Description : " + item.Description);
                    Console.WriteLine(product);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    GuestMenuPrint();
                }
            }
        }
    }
}
