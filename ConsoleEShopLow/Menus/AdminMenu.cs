﻿using ConsoleEShopLow.Interfaces;
using System;
using System.Linq;

namespace ConsoleEShopLow.Menus
{
    public class AdminMenu : IAdminMenu
    {
        readonly IProductService _productService;
        readonly IOrdersService _ordersService;
        readonly IProductRepository _productRepository;
        readonly IUserService _userService;
        readonly ICurrentUser _getCurrentUser;
        readonly IAuthorizationService _authorizationService;
        readonly IUserRepository _userRepository;
        
        public AdminMenu(IUserRepository userRepository,ICurrentUser currentUser, IUserService userService, IProductService productService, IOrdersService ordersService, IAuthorizationService authorizationService, IProductRepository productRepository)
        {
            _productService = productService;
            _ordersService = ordersService;
            _authorizationService = authorizationService;
            _productRepository = productRepository;
            _userService = userService;
            _getCurrentUser = currentUser;
            _userRepository = userRepository;
        }
        public void AdminMenuPrint()
        {
            Console.Clear();
            Console.WriteLine("     ------------------ Welcome  Admin ------------------");
            Console.WriteLine();
            Console.WriteLine("       If you want to view products,      press - 1");
            Console.WriteLine();
            Console.WriteLine("       If you want to search,             press - 2");
            Console.WriteLine();
            Console.WriteLine("       If you want to view  orders,       press - 3");
            Console.WriteLine();
            Console.WriteLine("       If you want to create order,       press - 4");
            Console.WriteLine();
            Console.WriteLine("       If you want to chunge information, press - 5");
            Console.WriteLine();
            Console.WriteLine("       If you want to add new product,    press - 6");
            Console.WriteLine();
            Console.WriteLine("       If you want to edit product,       press - 7");
            Console.WriteLine();
            Console.WriteLine("       If you want to log out,            press - 8");
            Console.WriteLine();
            Console.WriteLine("       If you want to chunge order status,press - 9");
            Console.WriteLine();
            Console.Write("       Select: ");

            string result;
            string[] possibleValues = new string[9] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            do
            {
                result = Console.ReadLine();
                Console.WriteLine();
                if (!possibleValues.Contains(result))
                {
                    Console.WriteLine(" Enter 1 or 2, or 3, or 4, or 5, or 6or 7,!!!!");
                }
            } while (!possibleValues.Contains(result));

            if (result == "1")
            {
                _productService.GetProducts();

                foreach (var item in _productService.GetProducts())
                {
                    string product = (" | Name : " + item.Name + " | Price : " + item.Price.ToString() + "$" + " | Category : " + item.CurrentCategory + " | Description : " + item.Description);
                    Console.WriteLine(product);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    AdminMenuPrint();
                }

            }
            if (result == "2")
            {
                Console.WriteLine();
                Console.Write("Search : ");
                string str = Console.ReadLine();
                Console.WriteLine();
                _productService.GetProductsByName(str);
                foreach (var item in _productService.GetProductsByName(str))
                {
                    string product = (" | Name : " + item.Name + " | Price : " + item.Price.ToString() + "$" + " | Category : " + item.CurrentCategory + " | Description : " + item.Description);
                    Console.WriteLine(product);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    AdminMenuPrint();
                }
            }
            if (result == "3")
            {
                foreach (var item in _ordersService.GetOrdersForAdmin())
                {
                    string order = ("| Create date :" + item.Date.ToString() + " | Order status : " + item.OrderStatus + " | Product : " +
                        _productRepository.GetProductById(item.ProductId).Name + " | Create by : "+ _getCurrentUser.GetCurrentUser().Login +" |");

                    Console.WriteLine(order);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string contin;
                do
                {
                    contin = Console.ReadLine();
                    if (contin != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (contin != "1");
                if (contin == "1")
                {
                    AdminMenuPrint();
                }
            }
            if (result == "4")
            {
                Console.WriteLine("Product name :");
                Console.WriteLine();
                string productName = Console.ReadLine();
                var productToOrder = _productService.GetProductsByName(productName);

                Console.WriteLine("Compleet !");

                _ordersService.CreateOrder(productToOrder.FirstOrDefault());


                Console.WriteLine();

                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                cont = Console.ReadLine();
                 if (cont != "1")
                 {
                   Console.WriteLine("If you want continue press 1 !");
                 }
                } while (cont != "1");
                   if (cont == "1")
                    {
                        AdminMenuPrint();
                    }
            }
            if (result == "5")
            {
                foreach (var item in _userRepository.GetUsers())
                {
                    Console.WriteLine("id = " + item.UserId.ToString()+ " | Name " + item.Login + " |");
                }
                Console.Write(" Enter user id :");
                string userID = Console.ReadLine();
                int id = Int32.Parse(userID);
                Console.WriteLine();
                Console.WriteLine("Login = " + _userRepository.GetUserById(id).Login + " | Password = " + _userRepository.GetUserById(id).Password);
                Console.WriteLine();
                Console.Write("Enter new Login : ");
                string newLogin = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter new password : ");
                string newPassword = Console.ReadLine();
                _userService.AdminChandeUserInfo(id,newLogin, newPassword);
                Console.WriteLine();
                Console.WriteLine("New Login = " + _userRepository.GetUserById(id).Login + " | Password = " + _userRepository.GetUserById(id).Password);
                Console.WriteLine();
                Console.WriteLine("If you want continue press 1 ");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    AdminMenuPrint();
                }

            }
            if (result == "6")
            {
                Console.Write("Enter product Name : ");
                string prodName = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter product Description : ");
                string prodDescript = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter product Prise : ");
                string prodPrice = Console.ReadLine();
                decimal price = decimal.Parse(prodPrice);
                Console.WriteLine();
                Console.WriteLine("Select 1 = Groceries | Select 2 = Chemicals | Select 3 = Vegetables | Select 4 = Fruits | ");
                
                string[] possibleStatus = new string[4] { "1", "2", "3", "4" };
                string select;
                do
                {
                    Console.WriteLine();
                    select = Console.ReadLine();
                    if (!possibleStatus.Contains(select))
                    {
                        Console.WriteLine(" Enter 1 or 2, or 3, or 4!!!!");

                    }
                } while (!possibleStatus.Contains(select));

                Category category = Category.Fruits;
                if (select == "1")
                {
                    category = Category.Groceries;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                if (select == "2")
                {
                    category = Category.Chemicals;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                if (select == "3")
                {
                    category = Category.Vegetables;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                if (select == "4")
                {
                    category = Category.Fruits;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }

                _productService.CreateProduct(prodName, category, prodDescript, price);

                Console.WriteLine("If you want continue press 1 ");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    AdminMenuPrint();
                }
            }
            if (result == "7")
            {
                Console.Write("enter Id :");
                string productId = Console.ReadLine();
                int prodId = int.Parse(productId);
                Console.WriteLine();
                Console.Write("Enter product Name : ");
                string prodName = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter product Description : ");
                string prodDescript = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter product Prise : ");
                string prodPrice = Console.ReadLine();
                decimal price = decimal.Parse(prodPrice);
                Console.WriteLine();
                Console.WriteLine("Select 1 = Groceries | Select 2 = Chemicals | Select 3 = Vegetables | Select 4 = Fruits | ");

                string[] possibleStatus = new string[4] { "1", "2", "3", "4" };
                string select;
                do
                {
                    Console.WriteLine();
                    select = Console.ReadLine();
                    if (!possibleStatus.Contains(select))
                    {
                        Console.WriteLine(" Enter 1 or 2, or 3, or 4!!!!");

                    }
                } while (!possibleStatus.Contains(select));

                Category category = Category.Fruits;
                if (select == "1")
                {
                    category = Category.Groceries;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                if (select == "2")
                {
                    category = Category.Chemicals;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                if (select == "3")
                {
                    category = Category.Vegetables;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                if (select == "4")
                {
                    category = Category.Fruits;
                    Console.WriteLine("Category : " + category);
                    Console.WriteLine();
                }
                _productService.EditProduct(prodId, prodName, category, prodDescript, price);
                
            }
            if (result == "8")
            {
                _authorizationService.LogOut();


                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    Console.Clear();
                    Console.WriteLine("Bye Bye !");

                }
            }
            if (result == "9")
            {
                foreach (var item in _ordersService.GetOrdersForAdmin())
                {
                    string order = (" | " + item.OrderId.ToString() + ")" + "| Create date :" + item.Date.ToString() + " | Order status : " + item.OrderStatus + " | Product : " +
                        _productRepository.GetProductById(item.ProductId).Name);

                    Console.WriteLine(order);
                    Console.WriteLine();
                }
                Console.WriteLine("Enter order number !");
                string number = Console.ReadLine();
                int numb = int.Parse(number);
                var currentOrder = _ordersService.GetOrderById(numb);
                Console.WriteLine("Select 1 = CanceledByAdmin | Select 2 = Paid | Select 3 = Sent | Select 4 = Completed | ");
                
                string[] possibleStatus = new string[4] { "1", "2", "3", "4" };
                string select;
                do
                {
                    Console.WriteLine();
                    select = Console.ReadLine();
                    if (!possibleStatus.Contains(select))
                    {
                        Console.WriteLine(" Enter 1 or 2, or 3, or 4!!!!");

                    }
                } while (!possibleStatus.Contains(select));
             

                if (select == "1")
                {
                    currentOrder.OrderStatus = OrderStatus.CanceledByAdmin;
                    Console.WriteLine("Status : " + currentOrder.OrderStatus);
                    Console.WriteLine();
                }
                if (select == "2")
                {
                    currentOrder.OrderStatus = OrderStatus.Paid;
                    Console.WriteLine("Status : " + currentOrder.OrderStatus);
                    Console.WriteLine();
                }
                if (select == "3")
                {
                    currentOrder.OrderStatus = OrderStatus.Sent;
                    Console.WriteLine("Status : " + currentOrder.OrderStatus);
                    Console.WriteLine();
                }
                if (select == "4")
                {
                    currentOrder.OrderStatus = OrderStatus.Completed;
                    Console.WriteLine("Status : " + currentOrder.OrderStatus);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    AdminMenuPrint();
                }
            }
        }
    }
}
             
        
            
