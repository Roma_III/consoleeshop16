﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShopLow.Services;

namespace ConsoleEShopLow.Interfaces.Menus
{
    public class MainMenu  : IMainMenu
    {
        readonly IloginMenu _iloginMenu;
        readonly IRegistrationMenu _registrationMenu;
        readonly IGuestMenu _guestMenu;
        public MainMenu(IloginMenu iloginMenu, IRegistrationMenu registrationMenu, IGuestMenu guestMenu)
        {
            _iloginMenu = iloginMenu;
            _registrationMenu = registrationMenu;
            _guestMenu = guestMenu;
        }
        public void Print()
        {
            Console.Clear();
            Console.WriteLine("       ---------------- Welcome to EShop -----------------");
            Console.WriteLine();
            Console.WriteLine(" |Login: Press - 1 | |Guest: Press - 2| |Register: Press - 3 |");
            Console.WriteLine();
            Console.Write("Chose: ");
           
            string result;
            string[] possibleValues = new string[3] {"1","2","3"};
            do
            { 
               result =  Console.ReadLine();
                Console.WriteLine();
                if (!possibleValues.Contains(result))
                {
                    Console.WriteLine(" Enter 1 or 2, or 3 !!!!");
                }
            } while (!possibleValues.Contains(result));

            if (result == "1")
            {
                _iloginMenu.Login();
            }
            if (result == "2")
            {
                _guestMenu.GuestMenuPrint();
            }
            if (result == "3")
            {
                _registrationMenu.Registration();
            }

        }
        
    }
}