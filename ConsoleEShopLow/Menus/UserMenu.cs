﻿using ConsoleEShopLow.Interfaces;
using System;
using System.Linq;

namespace ConsoleEShopLow.Menus
{
    public class UserMenu : IUserMenu
    {
        readonly IProductService _productService;
        readonly IOrdersService _ordersService;
        readonly IProductRepository _productRepository;
        readonly IUserService _userService;
        readonly ICurrentUser _getCurrentUser;
        readonly IAuthorizationService _authorizationService;
        public UserMenu(ICurrentUser currentUser,IUserService userService,IProductService productService, IOrdersService ordersService, IAuthorizationService authorizationService, IProductRepository productRepository)
        {
            _productService = productService;
            _ordersService = ordersService;
            _authorizationService = authorizationService;
            _productRepository = productRepository;
            _userService = userService;
            _getCurrentUser = currentUser;
        }
        
        public void UserMenuPrint()
        {
            Console.Clear();
            Console.WriteLine("     ------------------ Welcome  User ------------------");
            Console.WriteLine();
            Console.WriteLine("       If you want to view products,      press - 1");
            Console.WriteLine();
            Console.WriteLine("       If you want to search,             press - 2");
            Console.WriteLine();
            Console.WriteLine("       If you want to view your orders,   press - 3");
            Console.WriteLine();
            Console.WriteLine("       If you want to create order,       press - 4");
            Console.WriteLine();
            Console.WriteLine("       If you want to chunge information, press - 5");
            Console.WriteLine();
            Console.WriteLine("       If you want to log out,            press - 6");
            Console.WriteLine();
            Console.WriteLine("       If you want to chunge order status,press - 7");
            Console.WriteLine();
            Console.Write("       Select: ");

            string result;
            string[] possibleValues = new string[7] { "1", "2", "3", "4", "5", "6", "7"};
            do
            {
                result = Console.ReadLine();
                Console.WriteLine();
                if (!possibleValues.Contains(result))
                {
                    Console.WriteLine(" Enter 1 or 2, or 3, or 4, or 5, or 6or 7,!!!!");
                }
            } while (!possibleValues.Contains(result));

            if (result == "1")
            {
                _productService.GetProducts();

                foreach (var item in _productService.GetProducts())
                {
                    string product = (" | Name : " + item.Name + " | Price : " + item.Price.ToString() + "$" + " | Category : " + item.CurrentCategory + " | Description : " + item.Description);
                    Console.WriteLine(product);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    UserMenuPrint();
                }

            }
            if (result == "2")
            {
                Console.WriteLine();
                Console.Write("Search : ");
                string str = Console.ReadLine();
                Console.WriteLine();
                _productService.GetProductsByName(str);
                foreach (var item in _productService.GetProductsByName(str))
                {
                    string product = (" | Name : " + item.Name + " | Price : " + item.Price.ToString() + "$" + " | Category : " + item.CurrentCategory + " | Description : " + item.Description);
                    Console.WriteLine(product);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    UserMenuPrint();
                }
            }
            if (result == "3")
            {
                _ordersService.GetMyOrders();

                foreach (var item in _ordersService.GetMyOrders())
                {
                    string order = ("| Create date :" + item.Date.ToString() + " | Order status : " + item.OrderStatus +" | Product : " +
                        _productRepository.GetProductById(item.ProductId).Name);

                    Console.WriteLine(order);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    UserMenuPrint();
                }
            }
            if (result == "4")
            {
                Console.WriteLine("Product name :");
                Console.WriteLine();
                string productName = Console.ReadLine();
                var productToOrder = _productService.GetProductsByName(productName);

                Console.WriteLine("Compleet !");

                _ordersService.CreateOrder(productToOrder.FirstOrDefault());

                
                Console.WriteLine();
                
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    UserMenuPrint();
                }
            }
            if (result == "5")
            {
                Console.WriteLine("Login = " + _getCurrentUser.GetCurrentUser().Login + " | Password = " + _getCurrentUser.GetCurrentUser().Password);
                Console.WriteLine();
                Console.Write("Enter new Login : ");
                string newLogin = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter new password : ");
                string newPassword = Console.ReadLine();
                _userService.ChandeUserInfo(newLogin, newPassword);
                Console.WriteLine();
                Console.WriteLine("New Login = "+ _getCurrentUser.GetCurrentUser().Login + " | New Password = " + _getCurrentUser.GetCurrentUser().Password);
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    UserMenuPrint();
                }

            }
            if (result == "6")
            {
                _authorizationService.LogOut();
                
                
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    Console.Clear();
                    Console.WriteLine("Bye Bye !");  
                    
                }
            }
            if (result == "7")
            {
                foreach (var item in _ordersService.GetMyOrders())
                {
                    string order = ("| Create date :" + item.Date.ToString() + " | Order status : " + item.OrderStatus + " | Product : " +
                        _productRepository.GetProductById(item.ProductId).Name);

                    Console.WriteLine(order);
                    Console.WriteLine();
                }
                Console.WriteLine("Enter order number !");
                string number = Console.ReadLine();
                int numb = int.Parse(number);
                var currentOrder = _ordersService.GetOrderById(numb);
                Console.WriteLine("Select 1 = CanceledByUser | Select 2 = Received ");
                Console.WriteLine();
                string select = Console.ReadLine();
                if (select == "1")
                {
                    currentOrder.OrderStatus = OrderStatus.CanceledByUser;
                    Console.WriteLine("Status : " + currentOrder.OrderStatus);
                    Console.WriteLine();
                }
                else
                {
                    currentOrder.OrderStatus = OrderStatus.Received;
                    Console.WriteLine("Status : " + currentOrder.OrderStatus);
                    Console.WriteLine();
                }
                Console.WriteLine("Continue ? Press - 1");
                Console.WriteLine();
                string cont;
                do
                {
                    cont = Console.ReadLine();
                    if (cont != "1")
                    {
                        Console.WriteLine("If you want continue press 1 !");
                    }
                } while (cont != "1");
                if (cont == "1")
                {
                    UserMenuPrint();
                }
            }
        }
    }
}
