﻿using ConsoleEShopLow.Interfaces;
using System;
using System.Linq;

namespace ConsoleEShopLow.Menus
{
    public class RegistrationMenu : IRegistrationMenu
    {
        readonly IUserService _userService;
        readonly IloginMenu _iloginMenu;
        
        
        public RegistrationMenu(IUserService userService, IloginMenu iloginMenu)
        {
            _userService = userService;
            _iloginMenu = iloginMenu;
            
        }
        public void Registration()
        {
            Console.Clear();
            Console.WriteLine("---------------- Welcome to Registration -----------------");
            Console.WriteLine();
            Console.WriteLine("Enter your Login");
            Console.WriteLine();
            Console.Write("Login : ");
            string login;
            do
            {
                login = Console.ReadLine();
                Console.WriteLine();
                if (login == null)
                {
                    Console.WriteLine("Enter correct Login");
                    Console.Write("Login : ");

                }
            } while (login == null);

            Console.WriteLine("Enter your Password");
            Console.WriteLine();
            Console.Write("Password : ");

            string password;
            do
            {
                password = Console.ReadLine();
                Console.WriteLine();
                if (password == null)
                {
                    Console.WriteLine("Enter correct Password");
                    Console.Write("Password : ");

                }
            } while (password == null);

            _userService.Registration(login, password);
            Console.WriteLine("Complete");
            Console.WriteLine();
            Console.WriteLine("Go to login menu ?");
            Console.WriteLine();
            Console.WriteLine("OK - press 1, No - press 2");
            Console.WriteLine();
            Console.Write("Chose: ");

            string result;
            string[] possibleValues = new string[2] { "1", "2" };
            do
            {
                result = Console.ReadLine();
                Console.WriteLine();
                if (!possibleValues.Contains(result))
                {
                    Console.WriteLine(" Enter 1 or 2 !!!!");
                }
            } while (!possibleValues.Contains(result));

            if (result == "1")
            {
                _iloginMenu.Login();
            }
            if (result == "2")
            {
                Console.Clear();
                Console.WriteLine("Bye Bye !");
            }   
        }
    }
}
