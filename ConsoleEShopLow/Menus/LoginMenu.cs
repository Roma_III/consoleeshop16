﻿using ConsoleEShopLow.Interfaces;
using System;

namespace ConsoleEShopLow.Menus
{
    public  class LoginMenu : IloginMenu
    {
        readonly IAuthorizationService _authorizationService;
        readonly ICurrentUser _currentUser;
        readonly IUserMenu _userMenu;
        readonly IAdminMenu _AdminMenu;
        public LoginMenu(IAuthorizationService authorizationService, ICurrentUser currentUser, IUserMenu userMenu, IAdminMenu adminMenu)
        {
            _authorizationService = authorizationService;
            _currentUser = currentUser;
            _userMenu = userMenu;
            _AdminMenu = adminMenu;
        }
        public void Login()
        {
            Console.Clear();
            Console.WriteLine("---------------- Welcome to Login -----------------");
            Console.WriteLine("Enter your Login");
            Console.WriteLine();
            Console.Write("Login : ");
            string login;
            do
            {
                login = Console.ReadLine();
                Console.WriteLine();
                if (login == null)
                {
                    Console.WriteLine("Enter correct Login");
                    Console.Write("Login : ");

                }
            } while (login == null);

            Console.WriteLine("Enter your Password");
            Console.WriteLine();
            Console.Write("Password : ");

            string password;
            do
            {
                password = Console.ReadLine();
                Console.WriteLine();
                if (password == null)
                {
                    Console.WriteLine("Enter correct Password");
                    Console.Write("Password : ");

                }
            } while (password == null);

            _authorizationService.LogIn(login, password);

            if (_currentUser.GetCurrentUser().CurentRole == Role.User)
            {
                _userMenu.UserMenuPrint();
            }
            if (_currentUser.GetCurrentUser().CurentRole == Role.Admin)
            {
                _AdminMenu.AdminMenuPrint();
            }
            
        }
    }
}
